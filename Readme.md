## Create and Build Dockerfile
- Für Scaleway ist es wichtig, dass eine ENV PORT gesetzt wird (im Dockerfile), mit dem Port auf dem das Programm läuft. Dieser wird dann von Scaleway mit dem Port 80 gemappt: 
```
ENV PORT 8080
EXPOSE 8080
```
```
docker build --tag netflix:1.0 .
```

## Run Image 
```
docker run -p 80:8080 netflix:1.0 
```

## Upload to Scaleway Elements Registry 
### Login 
```
docker login rg.fr-par.scw.cloud/esbreg -u nologin -p 7ea68a3d-ffaf-4a08-b763-e4b4a5ccf639
```
### Upload 
```
docker pull netflix:1.0 
docker tag netflix:1.0  rg.fr-par.scw.cloud/esbreg/netflix:1.0 
docker push rg.fr-par.scw.cloud/esbreg/netflix:1.0 
```

## Deployment über Gitlab 
- Am besten ist es das Deployment über Gitlab zu machen, weil das gepackte Docker-Image relativ groß werden kann. 
- Die Deployment-Logik findet sich in der ```.gitlab-ci.yml```

