FROM bitnami/apache:latest

WORKDIR /app
COPY ./html .

ENV PORT 8080
EXPOSE 8080
